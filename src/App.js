import React from "react";
import Home from "./Components/Home/Home";
import Sidebar from "./Components/Sidebar/Sidebar";
import Main from "./Components/Main/Main";

export default function App() {
  return (
    <>
      <div className="flex h-screen">
        {/* Sidebar (30% width) */}
        <div className="w-30 bg-white border-e">
          <Sidebar/>
        </div>

        {/* Content (70% width) */}
        <div className="w-70 bg-gray-100">
          <Main/>
        </div>
      </div>
    </>
  );
}
